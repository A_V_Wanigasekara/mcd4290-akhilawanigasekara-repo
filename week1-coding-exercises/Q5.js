//Q5

// Any year entered returns as true as one of the conditions is true

let year;
let yearNot2015Or2016;

year = 2016;    //changed 2000 to 2016 (or 2015)

yearNot2015Or2016 = year !== 2015 && year !== 2016;   //changed || to &&

console.log(yearNot2015Or2016);